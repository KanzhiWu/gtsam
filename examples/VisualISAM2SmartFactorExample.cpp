/* ----------------------------------------------------------------------------

 * GTSAM Copyright 2010, Georgia Tech Research Corporation,
 * Atlanta, Georgia 30332-0415
 * All Rights Reserved
 * Authors: Frank Dellaert, et al. (see THANKS for the full author list)

 * See LICENSE for the license information

 * -------------------------------------------------------------------------- */

/**
 * @file    VisualISAM2SmartFactorExample.cpp
 * @brief   A visualSLAM example for the structure-from-motion problem on a simulated dataset
 * This version uses iSAM2 to solve the problem incrementally, with SmartFactor
 * @author  Kanzhi Wu
 */

/**
 * A structure-from-motion example with landmarks
 *  - The landmarks form a 10 meter cube
 *  - The robot rotates around the landmarks, always facing towards the cube
 */

// For loading the data
#include "SFMdata.h"

// Camera observations of landmarks (i.e. pixel coordinates) will be stored as Point2 (x, y).
#include <gtsam/geometry/Point2.h>

// Each variable in the system (poses and landmarks) must be identified with a unique key.
// We can either use simple integer keys (1, 2, 3, ...) or symbols (X1, X2, L1).
// Here we will use Symbols
#include <gtsam/inference/Symbol.h>

// We want to use iSAM2 to solve the structure-from-motion problem incrementally, so
// include iSAM2 here
#include <gtsam/nonlinear/ISAM2.h>

// iSAM2 requires as input a set set of new factors to be added stored in a factor graph,
// and initial guesses for any new variables used in the added factors
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/nonlinear/Values.h>

// In GTSAM, measurement functions are represented as 'factors'. Several common factors
// have been provided with the library for solving robotics/SLAM/Bundle Adjustment problems.
// Here we will use Projection factors to model the camera's landmark observations.
// Also, we will initialize the robot at some location using a Prior factor.
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/slam/ProjectionFactor.h>
#include <gtsam/nonlinear/NonlinearEquality.h>

#include <gtsam/slam/SmartProjectionPoseFactor.h>
#include <gtsam/slam/SmartProjectionFactor.h>
#include <gtsam/slam/BetweenFactor.h>

#include <boost/foreach.hpp>

#include <vector>
#include <map>

using namespace std;
using namespace gtsam;

/* ************************************************************************* */
int main(int argc, char* argv[]) {

  // Define the camera calibration parameters
  Cal3_S2::shared_ptr K(new Cal3_S2(50.0, 50.0, 0.0, 50.0, 50.0));

  // Define the camera observation noise model
  noiseModel::Isotropic::shared_ptr measurementNoise = noiseModel::Isotropic::Sigma(2, 1.0); // one pixel in u and v
  const noiseModel::Isotropic::shared_ptr model = noiseModel::Isotropic::Sigma(2,1);

  // Create the set of ground-truth landmarks
  vector<Point3> points = createPoints();
  cout << "Load " << points.size() << " landmarks\n";


  // Create the set of ground-truth poses
  vector<Pose3> poses = createPoses();
  cout << "Load " << poses.size() << " poses\n";

  // Create an iSAM2 object
  ISAM2Params parameters;
  parameters.relinearizeThreshold = 0.01;
  parameters.relinearizeSkip = 1;
  ISAM2 isam(parameters);

  // Create a Factor Graph and Values to hold the new data
  NonlinearFactorGraph graph;

  // add prior on the first pose
  noiseModel::Diagonal::shared_ptr poseNoise = noiseModel::Diagonal::Sigmas((Vector(6) << Vector3::Constant(3),Vector3::Constant(1)).finished()); // 30cm std on x,y,z 0.1 rad on roll,pitch,yaw

  Values initialEstimate;
  Values landmarkEstimates = initialEstimate; // copy landmarks
  bool setinit = true;

  // noised 1st pose
  Pose3 npose0 = poses[0].compose(Pose3(Rot3::Rodrigues(-0.5, 0.5, 0.5), Point3(0.05, -0.10, 0.20)));
  initialEstimate.insert( Symbol('x', 0), npose0 );


  /// ----------------------------------------------------
  /// A. fix the first pose to the one with error
  ///
//  Pose3 firstPose = initialEstimate.at<Pose3>(Symbol('x', 0));
//  graph.push_back(NonlinearEquality<Pose3>(Symbol('x', 0),firstPose));


  /// ----------------------------------------------------
  /// B. add first pose with prior uncertainty
  graph.push_back( PriorFactor<Pose3>(Symbol('x', 0), poses[0], poseNoise) );




  // map of all factors
  typedef SmartProjectionPoseFactor<Cal3_S2> SmartFactor;
  map<size_t, SmartFactor::shared_ptr> used_factors;
  for ( size_t i = 0; i < points.size(); ++ i ) {
    SmartFactor::shared_ptr factor(new SmartFactor(model, K));
    used_factors[i] = factor;
    graph.push_back( factor ) ;
  }

  for ( size_t i = 0; i < poses.size(); ++ i ) {



    if ( i == 0 ) {
      /// ---------------------------------------------
      /// add prior to the first landmark
//      noiseModel::Isotropic::shared_ptr pointNoise = noiseModel::Isotropic::Sigma(3, 0.1);
//      graph.push_back(PriorFactor<Point3>(Symbol('l', 0), points[0], pointNoise)); // add directly to graph
//      initialEstimate.insert(Symbol('l', 0), points[0]);

//      graph.push_back( PriorFactor<Pose3>(Symbol('x', i), poses[i], poseNoise) );

//      initialEstimate.insert(Symbol('x', i), poses[i].compose(Pose3(Rot3::Rodrigues(-0.1, 0.2, 0.25), Point3(0.05, -0.10, 0.20))));

      for ( size_t j = 0; j < points.size(); ++ j ) {
        // get 2d measurement
        SimpleCamera camera(poses[i], *K);
        Point2 measurement = camera.project(points[j]);
        /// -------------------------------------
        ///  add measurement to each factor
        used_factors[j]->add( measurement, Symbol('x', i) );
      }

//      continue;
    }
    else {
      /// --------------------------------------
      /// C. add prior pose (EACH POSE) to the graph
//      if ( i <= 2 ) /// define the number of the added prior
//        graph.push_back( PriorFactor<Pose3>(Symbol('x', i), poses[i], poseNoise) );
        graph.push_back( PriorFactor<Pose3>(Symbol('x', i),
                                            poses[i].compose(Pose3(Rot3::Rodrigues(-0.1, 0.2, 0.25), Point3(1,1,1))),
                                            poseNoise) );



      /// -------------------------------------
      /// add pose to estimation initial value, with error
      initialEstimate.insert(Symbol('x', i), poses[i].compose(Pose3(Rot3::Rodrigues(-0.1, 0.2, 0.25), Point3(0.05, -0.10, 0.20))));


      // add observation for smart factor
      for ( size_t j = 0; j < points.size(); ++ j ) {
        // get 2d measurement
        SimpleCamera camera(poses[i], *K);
        Point2 measurement = camera.project(points[j]);
        /// -------------------------------------
        ///  add measurement to each factor
        used_factors[j]->add( measurement, Symbol('x', i) );
      }


      /// ---------------------------------------
      /// print the inital value
  //    initialEstimate.print("Initial estimate: ");
      // Update iSAM with the new factors
      isam.update(graph, initialEstimate);
//      isam.print( "isam:\n" );

      Values currentEstimate = isam.calculateEstimate();
      currentEstimate.print("\nCurrent estimate: ");


      graph.resize(0);
      initialEstimate.clear();

      if ( setinit ) {
        /// --------------------------------------
        /// set initial value only once at the second step!!!
        /// I do NOT know how to update the initial value
        /// from the given example

        std::cout << "set initial value\n";
        for ( size_t jj = 0; jj < points.size(); ++ jj ) {
          SmartFactor::shared_ptr smft = boost::dynamic_pointer_cast<SmartFactor>(used_factors[jj]);
          boost::optional< Point3 > lm = smft->point( currentEstimate );
          initialEstimate.insert(symbol('l', jj), *lm);
        }
        setinit = false;
      }

      landmarkEstimates = Values();
      for ( size_t jj = 0; jj < points.size(); ++ jj ) {
        SmartFactor::shared_ptr smft = boost::dynamic_pointer_cast<SmartFactor>(used_factors[jj]);
        boost::optional< Point3 > lm = smft->point( currentEstimate );
        std::cout << "\npoint " << jj << ": " << (*lm);
      }
    }


//    BOOST_FOREACH(const Values::ConstFiltered<Point3>::KeyValuePair& it, currentEstimate.filter<Point3>())
//      std::cout  << "* " << it.value.x() << "\t" << it.value.y() << "\t" << it.value.z() << std::endl;
//    BOOST_FOREACH(const Values::ConstFiltered<Pose3>::KeyValuePair& it, currentEstimate.filter<Pose3>())
//      std::cout  << it.value.matrix() <<  std::endl;

    cout << "\n****************************************************\n";
    getchar();
  }
  return 0;
}
/* ************************************************************************* */
